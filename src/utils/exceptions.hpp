#ifndef HIDE_AND_SEEK_EXCEPTIONS_HPP
#define HIDE_AND_SEEK_EXCEPTIONS_HPP

#include <exception>
#include <string>

class ClassNotFound : public std::exception
{
    public:

        const char * what() const throw ()
        {
            return "Expected class not found";
        }
};

#endif