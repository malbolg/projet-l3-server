#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_EXPERT_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_EXPERT_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief Expert Seeker class
 * 
 */
class ExpertSeeker : public Seeker
{
public:
    ExpertSeeker(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);

    ~ExpertSeeker();
};

#endif