#include <deque>
#include "player/player.hpp"
#include "utils/randomGenerator.hpp"
#include "game.hpp"
#include "maze/block/material/material.hpp"

/**
 * @brief Construct a new Player:: Player object
 * 
 * @param name 
 */
Player::Player(const std::string& name) : name(name) {

	// Retrieve the current random generator
	std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
	std::vector<Orientation> orientations = PlayerAction::getOrientations();
	this->orientation = orientations[rGenerator->getRandomInt(orientations.size() - 1)];
}

/**
 * @brief Return the all the possible actions of the specific player
 * 
 * @return const std::vector<PlayerAction>& 
 */
const std::vector<PlayerAction>& Player::getPossibleActions() const {
	return this->possibleActions;
}

/**
 * @brief Return the name of the player
 * 
 * @return const std::string& 
 */
const std::string& Player::getName() const {
	return name;
}

/**
 * @brief Return the current location of the player
 * 
 * @return const Point& 
 */
const Point& Player::getLocation() const {
	return location;
}

/**
 * @brief Update the location of the player into the Maze
 * 
 * @param x 
 * @param y 
 */
void Player::setLocation(const unsigned int x, const unsigned int y) {
	location = Point(x, y);
}

/**
 * @brief Retrieve the current player orientation
 * 
 * @return Orientation 
 */
const Orientation Player::getOrientation() const {
	return orientation;
}

/**
 * @brief Update the orientation of the player
 * 
 * @param orient 
 */
void Player::setOrientation(Orientation orient) {
	this->orientation = orient;
}

/**
 * @brief return a JSOn representation of player
 * 
 * @return nlohmann::json 
 */
nlohmann::json Player::toJSON() const {

	nlohmann::json json;
	nlohmann::json json_role;

	json["role"] = getRole();
	json["roleStr"] = roleToString(getRole());
	json["name"] = this->getName();
	json["orientation"] = orientation;
	json["orientationStr"] = orientationToString(orientation);
	json["location"] = location.toJSON();

	return json;
}

/**
 * @brief Specific display of the player
 * 
 * @return const std::string 
 */
const std::string Player::toString() const {
	return "{role:" + roleToString(this->getRole()) + ", name: " + this->getName() + ", view: " +
		   orientationToString(orientation) + "}";
}

/**
 * @brief Retrieve the current location of last seen opponent
 *
 * @return Point
 */
const Point & Player::getTargetLoc() const {
    return targetLoc;
}

/**
 * @brief Reset the location of last seen opponent
 *
 * @param loc
 */
void Player::forgetTargetLoc() {
    targetLoc = Point(-1,-1);
}

/**
 * @brief Search for opponent and stores it using setTargetLoc
 *
 * @param observation, role
 */
void Player::setTargetLoc(const std::shared_ptr<Observation> &observation) {

    double distance = INFINITY;
    std::shared_ptr<Player> target;
    Role roleOpponent = SEEKER;

    // Si player est seeker
    if (this->getRole() == SEEKER) {
        roleOpponent = HIDER;
    }

    for (const auto & cell : observation->getCells()) {
        if (cell->hasPlayer() and cell->getPlayer()->getRole() == roleOpponent) {
            // Opponent vu

            double dist_h_s = sqrt(pow(cell->getPlayer()->getLocation().x - cell->getPlayer()->getLocation().y, 2) + pow(this->getLocation().x - this->getLocation().y, 2));

            if (dist_h_s < distance) {
                distance = dist_h_s;
                target = cell->getPlayer(); // On met le Opponent le + proche dans target
            }

        }
    }

    if (target != nullptr) {
        //this->setTargetLoc(target->getLocation()); // Si on a trouvé une target, on sauvegarde sa pos
        targetLoc = target->getLocation();
    }

}

/**
 * @brief Retrieve the action index to make in relation to the target
 *
 * @param observation, actions
 *
 * @return int
 */
int Player::chooseAction(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions, const PlayerAction &optimal) const {

    //Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    //PlayerAction optimal = this->getActionFromTarget();

    std::vector<int> check;
    check.push_back(this->findAction(optimal, actions)); // Optimal

    if (this->getRole() == 1) { // Hider

        check.push_back(this->findAction(PlayerAction(this->inverseOrientation(optimal.orientation), Interaction(PLACE)),actions)); // Place

        check.push_back(this->findAction(PlayerAction(optimal.orientation, Interaction(CARRY)), actions)); // Carry
        Orientation adjacent = this->getAdjacentOrientation(optimal.orientation, actions);
        check.push_back(this->findAction(PlayerAction(adjacent, Interaction(CARRY)), actions)); // Carry Adjacent
        check.push_back(this->findAction(PlayerAction(this->inverseOrientation(adjacent), Interaction(CARRY)), actions)); // Carry Adjacent Inverse


        check.push_back(this->findAction(PlayerAction(this->getAdjacentOrientation(optimal.orientation, actions), optimal.interaction), actions)); // Move adjacent

    } else { // Seeker

        // Hit
        int hitIndex = this->findAction(PlayerAction(optimal.orientation, Interaction(HIT)), actions);
        if (hitIndex != -1) {

            int playerIndex;
            for (uint i=0; i < observation->getCells().size(); i++) {

                if (observation->getCells().at(i)->hasPlayer() and observation->getCells().at(i)->getPlayer()->getName() == this->getName()) {
                    playerIndex = i;
                }

            }

            int blocFrontIndex;
            switch(optimal.orientation) {

                case 0:
                    blocFrontIndex = playerIndex - std::get<0>(observation->getSize());
                    break;
                case 1:
                    blocFrontIndex = playerIndex + std::get<0>(observation->getSize());
                    break;
                case 2:
                    blocFrontIndex = playerIndex - 1;
                    break;
                case 3:
                    blocFrontIndex = playerIndex + 1;
                    break;

            }

            // Si bloc pas movable ou bloc de fer, on ne tape pas
            std::shared_ptr<Block> blocEnFace = observation->getCells().at(blocFrontIndex)->getBlock();
            bool metal = false;
            if (blocEnFace->getKind() == MATERIAL) {
                auto material = std::dynamic_pointer_cast<Material>(blocEnFace);
                if (material->getMaterialKind() == MaterialKind::METAL) {
                    metal = true;
                }
            }
            if (!blocEnFace->isMovable() or metal) {
                hitIndex = -1;
            }

        }

        check.push_back(hitIndex);

    }

    check.push_back(rGenerator->getRandomInt(actions.size() - 1)); // Random

    int final;
    for (const auto & a : check) {
        if (a != -1) {
            final = a;
            break;
        }
    }

    return final;

}

/**
 * @brief Inverse Orientation
 *
 * @param orientation
 *
 * @return Orientation
 */
Orientation Player::inverseOrientation(const Orientation orientation) const {

    switch(orientation) {
        case TOP:
            return Orientation(BOTTOM);
        case BOTTOM:
            return Orientation(TOP);
        case LEFT:
            return Orientation(RIGHT);
        case RIGHT:
            return Orientation(LEFT);
        default:
            return orientation;
    }

}

/**
 * @brief Get adjacent Orientation (ex up or down if input left or right)
 *
 * @param orientation
 *
 * @return Orientation
 */
Orientation Player::getAdjacentOrientation(const Orientation orientation, const std::vector<PlayerAction> &actions) const {

    bool up = false;
    bool down = false;
    bool left = false;
    bool right = false;

    for (auto action : actions) {
        if (action.interaction == MOVE) {
            switch (action.orientation) {
                case TOP:
                    up = true;
                    break;
                case BOTTOM:
                    down = true;
                    break;
                case LEFT:
                    left = true;
                    break;
                case RIGHT:
                    right = true;
                    break;
            }
        }
    }

    switch(orientation) {
        case TOP:
            if (left and GameParams::width/2 < this->getLocation().x) {
                return Orientation(LEFT);
            } else {
                return Orientation(RIGHT);
            }
        case BOTTOM:
            if (right and GameParams::width/2 > this->getLocation().x) {
                return Orientation(RIGHT);
            } else {
                return Orientation(LEFT);
            }
        case LEFT:
            if (down and GameParams::height/2 > this->getLocation().y) {
                return Orientation(BOTTOM);
            } else {
                return Orientation(TOP);
            }
        case RIGHT:
            if (up and GameParams::height/2 < this->getLocation().y) {
                return Orientation(TOP);
            } else {
                return Orientation(BOTTOM);
            }
        default:
            return orientation;
    }

}

/**
 * @brief Finds action index in action list
 *
 * @param action, actions
 *
 * @return int
 */
int Player::findAction(const PlayerAction &action, const std::vector<PlayerAction> &actions) const {

    for (uint i=0; i < actions.size(); i++) {
        if (actions.at(i).orientation == action.orientation and actions.at(i).interaction == action.interaction) {
            return i; // Si action voulu est dispo, alors on return son index
        }
    }

    return -1;

}

/**
 * @brief Retrieve the action to make from an observation
 *
 * @param observation
 *
 * @return PlayerAction
 */
PlayerAction Player::getActionFromTarget() const {

    // Si on a une pos target en mémoire
    if (this->getTargetLoc().x != -1) {
        return {getOrientationToPoint(this->getLocation(), this->getTargetLoc()), Interaction(MOVE)};
    } else {
        return {Orientation(-1), Interaction(-1)};
    }
}

/**
 * @brief Get necessary orientation to get to a Point
 *
 * @param point
 *
 * @return Orientation
 */
Orientation Player::getOrientationToPoint(const Point start, const Point end) const {
    int distx = end.x - start.x;
    int disty = end.y - start.y;

    Orientation chooseOrientation;

    // Si hider
    if (this->getRole() == HIDER) {
        if (abs(distx) >= abs(disty)) {
            if (distx > 0) {
                chooseOrientation = Orientation(LEFT);
            } else {
                chooseOrientation = Orientation(RIGHT);
            }
        } else {
            if (disty > 0) {
                chooseOrientation = Orientation(TOP);
            } else {
                chooseOrientation = Orientation(BOTTOM);
            }
        }
    } else {
        if (abs(distx) >= abs(disty)) {
            if (distx > 0) {
                chooseOrientation = Orientation(RIGHT);
            } else {
                chooseOrientation = Orientation(LEFT);
            }
        } else {
            if (disty > 0) {
                chooseOrientation = Orientation(BOTTOM);
            } else {
                chooseOrientation = Orientation(TOP);
            }
        }
    }

    return chooseOrientation;
}

/**
 * @brief Destroy the Player:: Player object
 * 
 */
Player::~Player() {}

std::deque<Point> Player::Path(std::unordered_map<Point, Point>& Parents, Point& current) {
	std::deque<Point> path;
	path.push_front(current);

	auto current_it = Parents.find(current);

	do {
		path.push_front(current_it->second);
	} while ((current_it = Parents.find(current_it->second)) != Parents.end());


	return path;
}

/**
 * @brief A distributed a_star algorithm that uses an internal memory of the maze and a cost function to navigate around the maze
 * @param A The start coords
 * @param B The end coords
 * @param cost_function The cost function, returns the cost to move between a cell and another, estimated
 * @returns the first coordinate of the path ( as the path is subject to change over time due to the maze's configuration )
 * @returns B if no path was found
 * @par Note: it is best to consider the cost_function to be the cost between adjacent cells
 */
Point Player::a_star(const Point &A, const Point &B, std::function<double(const Point &, const Point &)> cost_function) {
//	Le set des nodes ayant besoin d'être "(re)étendue"
	std::deque<Point> open_set;
	open_set.push_back(A);

//	parent des divers noeud.
	std::unordered_map<Point, Point> parent_path;


//	coût de trajet au divers noeud connus
	std::unordered_map<Point, double> global_node_cost;

//	Estimation du meilleur cout au noeud
	std::unordered_map<Point, double> heuristic_node_cost;

//	Initialisation
	for(int i = 1; i < GameParams::height - 1; i++) {
		for(int j = 1; j < GameParams::width - 1; j++) {
			auto new_point = Point(j, i);

			global_node_cost[new_point] = std::numeric_limits<double>::max();
			heuristic_node_cost[new_point] = std::numeric_limits<double>::max();
		}
	}

	global_node_cost[A] = 0;
	heuristic_node_cost[A] = cost_function(A, B);

	Point current;
	double dist;
	while(!open_set.empty()) {
		auto current_it = std::min_element(open_set.cbegin(), open_set.cend(),
										   [&heuristic_node_cost](const Point& lhs, const Point& rhs) {
											   return heuristic_node_cost[lhs] < heuristic_node_cost[rhs];
										   });
		current = *current_it;
		if(current == B) {
			return Player::Path(parent_path, current)[1];
		}

		open_set.erase(current_it);

		std::deque<Point> v_neighboors = expand_point(current);
		for(auto& valid_neighboor: v_neighboors) {
			int move_to_neighboor_cost= cost_function(current, valid_neighboor);

//			on considère que l'on ne peut pas bouger sur le noeud si l'heuristique de déplacement
//			retourne une valeur infinie, il devient invalide
			if( move_to_neighboor_cost == std::numeric_limits<double>::max() )
				continue;


			dist = global_node_cost[current] + move_to_neighboor_cost;

			if(dist < global_node_cost[valid_neighboor]) {
				parent_path[valid_neighboor] = current;
				global_node_cost[valid_neighboor] = dist;
				heuristic_node_cost[valid_neighboor] = dist + cost_function(A, valid_neighboor);

				if(std::find(open_set.cbegin(), open_set.cend(), valid_neighboor) == open_set.cend())
					open_set.push_back(valid_neighboor);
			}
		}
	}

	return B;
}

std::deque<Point> Player::expand_point(Point& P) {
	std::deque<Point> queue;
	if(P.x < GameParams::width - 1)
		queue.push_front(Point(P.x + 1, P.y));

	if(P.x > 1)
		queue.push_front(Point(P.x - 1, P.y));

	if(P.y < GameParams::height - 1)
		queue.push_front(Point(P.x, P.y + 1));

	if(P.y > 1)
		queue.push_front(Point(P.x, P.y - 1));

	return queue;
}

/**
 * @brief Update the maze in memory with an observation
 *
 * @param observation
 *
 */
void Player::updateMazeMem(const std::shared_ptr<Observation> &observation) {

    if (mazeMem.empty()) {
        mazeMem.resize(observation->getCells().size());
    }

    for (int i=0; i < observation->getCells().size(); i++) {

        std::shared_ptr<MazeCell> observationCell = observation->getCells().at(i);

        if (mazeMem.at(i) == nullptr) {
            std::shared_ptr<MazeCell> cell(new MazeCell(observationCell->getBlock(), observationCell->getPlayer()));
            mazeMem.at(i) = cell;
        }


        if (observationCell->getBlock()->getKind() != BKind(UNDEFINED)) {
            mazeMem.at(i)->setBlock(observationCell->getBlock());
        }

        mazeMem.at(i)->setPlayer(observationCell->getPlayer());

    }
}

const std::vector<std::shared_ptr<MazeCell>> &Player::getMazeMem() const {
    return mazeMem;
}

std::shared_ptr<MazeCell> Player::getCellFromPoint(const Point point) const {
    return mazeMem.at((GameParams::width * point.y) + point.x);
}

