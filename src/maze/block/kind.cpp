#include "maze/block/kind.hpp"

std::string blockKindToString(enum BKind kind) {
    switch (kind)
    {
    case BORDER:
        return "+";
        break;
    case GROUND:
        return "_";
        break;
    case MATERIAL:
        return "X";
        break;   
    case UNDEFINED:
        return "U";
        break;         
    default:
        return "-";
        break;
    }
}

std::string blockKindToStringForJSON(enum BKind kind) {
    switch (kind)
    {
    case BORDER:
        return "BORDER";
        break;
    case GROUND:
        return "GROUND";
        break;
    case MATERIAL:
        return "MATERIAL";
        break;   
    case UNDEFINED:
        return "UNDEFINED";
        break;         
    default:
        return "-";
        break;
    }
}