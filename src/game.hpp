#ifndef HIDE_AND_SEEK_GAME_HPP
#define HIDE_AND_SEEK_GAME_HPP

#include <memory>
#include <tuple>
#include "maze/maze.hpp"

namespace GameParams
{
    const int height = 20, width = 20;
    const int nRounds = 200;
    const int nSeekers = 2;
    const int nHiders = 2;

    const double pctBlock = 0.40; // 40% of whole maze can be composed of blocks
    const double pctWood = 0.08; // 8% of whole maze
    const double pctStone = 0.04; // 4% of whole maze
    const double pctMetal = 0.02; // 2% of whole maze
}

/**
 * @brief Keep information about client address (kind of player and players)
 * 
 */
class ClientPlayer {

    private:
        int port;
        std::string address;
        std::string className;
        std::vector<std::shared_ptr<Player>> players;
        Role role;

    public:
        ClientPlayer(int port, const std::string address, const std::string className, Role role); 

        bool addPlayer(const std::shared_ptr<Player> &player);
        std::string getURL();
        std::string getAddress() const;
        int getPort() const;
        const std::string getClassName() const;
        Role getRole() const;
        const std::vector<std::shared_ptr<Player>> &getPlayers();
};

/**
 * @brief Game class which manages how the game process
 * 
 */
class Game
{
private:
    unsigned int maxRounds;
    unsigned int nround;
    std::shared_ptr<Maze> maze;
    std::shared_ptr<ClientPlayer> hiderClient;
    std::shared_ptr<ClientPlayer> seekerClient;
    std::shared_ptr<Player> currentPlayer;
    std::vector<std::shared_ptr<Player>> players;

    // private because Game only manage which player is next
    const std::shared_ptr<Player> getNextPlayer();

public:
    Game(const std::shared_ptr<Maze> &maze, const unsigned int &maxRounds, const std::shared_ptr<ClientPlayer> &hider, const std::shared_ptr<ClientPlayer> &seeker);

    // constructor without Client usage
    Game(const std::shared_ptr<Maze> &maze, const unsigned int &maxRounds);

    void addPlayer(const std::shared_ptr<Player> &player);

    const std::pair<std::shared_ptr<Observation>, std::vector<PlayerAction>> getState(const std::shared_ptr<Player> &player) const;
    const std::shared_ptr<Observation> step(const std::shared_ptr<Player> &player, const PlayerAction &action);
    const bool end() const;

    const std::shared_ptr<Player> &getCurrentPlayer() const;
    const std::vector<std::shared_ptr<Player>> getPlayers(Role role) const;
    const std::shared_ptr<Maze> &getMaze() const;
    const unsigned int &getNumberOfRounds() const;
    const unsigned int getNumberOfPlayers() const;
    const std::shared_ptr<ClientPlayer> &getClient(Role role) const;
    const nlohmann::json toJSON() const;
    Role getCurrentWinner() const;
    
    ~Game();
};


#endif

