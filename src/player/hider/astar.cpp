#include "player/hider/astar.hpp"
#include "utils/randomGenerator.hpp"
#include "maze/block/material/material.hpp"

#include <vector>

/**
 * @brief Construct a new AstarHider:: AstarHider object
 * 
 * 
 * @param name 
 */
AstarHider::AstarHider(const std::string& name) : Hider(name) {}

/**
 * @brief Destroy the Hider:: Hider object
 * 
 */
AstarHider::~AstarHider() {};

/**
 * @brief Return a astar action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction& AstarHider::play(const std::shared_ptr<Observation>& observation,
									 const std::vector<PlayerAction>& actions) const {

	//Retrieve the current random generator
	std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

	PlayerAction optimalAction;
	Point optimalPoint;
	Point target = Point(-1, -1);
	std::vector<Point> seekers, pred_seeker_pos;

	for(const auto& c: getMazeMem()) {
		if(c->hasPlayer() && c->getPlayer()->getRole() == Role::SEEKER) {
			seekers.push_back(c->getPlayer()->getLocation());
		}
	}

	if(this->getTargetLoc().x != -1) { // On a une target

		optimalAction = PlayerAction(this->getActionFromTarget().orientation,
                                     this->getActionFromTarget().interaction);

	} else if(!seekers.empty()) { // On a des seekers
		for(auto& seek: seekers) {
			pred_seeker_pos.push_back(seek);
			for(const auto& next_turn_positions: expand_point(seek)) {
				auto cell_what = getCellFromPoint(next_turn_positions);

				if(cell_what->getBlock()->getKind() == BKind::GROUND)
					pred_seeker_pos.push_back(next_turn_positions);

			}
		}

		if(std::find(pred_seeker_pos.begin(), pred_seeker_pos.end(), this->getLocation()) != pred_seeker_pos.end()) {
			for( auto &act: actions ) {
				if(	act.interaction == Interaction::MOVE ){
					Point P(this->getLocation().x, this->getLocation().y);
					switch( act.orientation ) {
						case Orientation::TOP:
							P.y= this->getLocation().y+1;
							P.x= this->getLocation().x;
							if(std::find(pred_seeker_pos.begin(), pred_seeker_pos.end(), P) !=
							   pred_seeker_pos.end())
                                optimalAction = act;
						break;

						case Orientation::BOTTOM:
							P.y= this->getLocation().y-1;
							P.x= this->getLocation().x;
							if(std::find(pred_seeker_pos.begin(), pred_seeker_pos.end(), P) !=
							   pred_seeker_pos.end())
                                optimalAction = act;
						break;

						case Orientation::LEFT:
							P.x= this->getLocation().x-1;
							P.y= this->getLocation().y;
							if(std::find(pred_seeker_pos.begin(), pred_seeker_pos.end(), P) !=
							   pred_seeker_pos.end())
                                optimalAction = act;
						break;

						case Orientation::RIGHT:
							P.x= this->getLocation().x+1;
							P.y= this->getLocation().y;
							if(std::find(pred_seeker_pos.begin(), pred_seeker_pos.end(), P) !=
							   pred_seeker_pos.end())
                                optimalAction = act;
						break;

						default: break;
					}
					break;
				}
			}

		}
	} else if(!this->hasBlock()) {

		// Recherche bloc le plus proche
		double distance = INFINITY;

		for(int i = 0; i < getMazeMem().size(); i++) {

			int width = std::get<0>(observation->getSize());
			Point blocPoint = Point(i % width, i / width);
			std::shared_ptr<Block> bloc = getCellFromPoint(blocPoint)->getBlock();
			if(bloc->getKind() == MATERIAL) {
				auto material = std::dynamic_pointer_cast<Material>(bloc);
				if(material->getMaterialKind() == MaterialKind::METAL) {
					double dist_h_s = sqrt(pow(blocPoint.x - blocPoint.y, 2) +
										   pow(this->getLocation().x - this->getLocation().y, 2));
					if(dist_h_s < distance) {
						distance = dist_h_s;
						target = blocPoint;
					}
				}
			}
		}

		optimalPoint = Player::a_star(this->getLocation(), target,
									  [this](const Point& Start, const Point& End) -> double {

										  return End - Start + (End.x < 10? 999 : 0);

									  });

        optimalAction = PlayerAction(this->getOrientationToPoint(this->getLocation(), optimalPoint),
                                     Interaction(MOVE));

	}

	return actions[this->chooseAction(observation, actions, optimalAction)];

}

void AstarHider::update(const std::shared_ptr<Observation>& prev,
						const std::shared_ptr<Observation>& next, const PlayerAction& action) {

	this->setTargetLoc(prev); // Target pos seeker

	//MazeMem
	this->updateMazeMem(prev);
};