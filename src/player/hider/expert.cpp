#include "player/hider/expert.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>
#include <unistd.h>

/**
 * @brief Construct a new ExpertHider:: ExpertHider object
 * 
 * 
 * @param name 
 */
ExpertHider::ExpertHider(const std::string &name) : Hider(name){}


/**
 * @brief Destroy the Hider:: Hider object
 * 
 */
ExpertHider::~ExpertHider() {};

/**
 * @brief Return a expert action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &ExpertHider::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    PlayerAction optimal = this->getActionFromTarget();
    return actions[this->chooseAction(observation, actions, optimal)];

}

void ExpertHider::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {

    /*if (this->getActionFromTarget(prev).orientation != action.orientation or this->getActionFromTarget(prev).interaction != action.interaction) {
        this->forgetTargetLoc();
    }*/

    this->setTargetLoc(prev);

};