#include "player/seeker/expert.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>

/**
 * @brief Construct a new ExpertSeeker:: ExpertSeeker object
 * 
 * 
 * @param name 
 */
ExpertSeeker::ExpertSeeker(const std::string &name) : Seeker(name){}


/**
 * @brief Destroy the Seeker:: Seeker object
 * 
 */
ExpertSeeker::~ExpertSeeker() {}

/**
 * @brief Return a expert action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &ExpertSeeker::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    PlayerAction optimal = this->getActionFromTarget();
    return actions[this->chooseAction(observation, actions, optimal)];

}

void ExpertSeeker::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {

    if ((this->getLocation().x == this->getTargetLoc().x) and (this->getLocation().y == this->getTargetLoc().y)) {
        this->forgetTargetLoc(); // Suppression de la target si on y est
    }

    this->setTargetLoc(prev);

}