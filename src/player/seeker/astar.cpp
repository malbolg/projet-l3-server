#include "player/seeker/astar.hpp"
#include "utils/randomGenerator.hpp"
#include "maze/block/material/material.hpp"

#include <vector>

/**
 * @brief Construct a new astarSeeker:: astarSeeker object
 *
 *
 * @param name
 */
AstarSeeker::AstarSeeker(const std::string &name) : Seeker(name){}


/**
 * @brief Destroy the Seeker:: Seeker object
 *
 */
AstarSeeker::~AstarSeeker() {}

/**
 * @brief Return a astar action to do
 *
 * @param state
 * @return Action
 */
const PlayerAction &AstarSeeker::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    PlayerAction optimalAction;
    Point optimalPoint;

    Point target = Point(-1,-1);

    if (this->getTargetLoc().x != -1) { // Si on a une target

        target = this->getTargetLoc();

    } else { // Si pas de target

        // Recherche bloc le plus proche
        double distance = INFINITY;

        for (int i=0; i < getMazeMem().size(); i++) {

            int width = std::get<0>(observation->getSize());
            Point blocPoint = Point(i % width, i / width);

            std::shared_ptr<Block> bloc = getCellFromPoint(blocPoint)->getBlock();
            if (bloc->getKind() == MATERIAL) {
                auto material = std::dynamic_pointer_cast<Material>(bloc);
                if (material->getMaterialKind() == MaterialKind::WOOD) {
                    double dist_h_s = sqrt(pow(blocPoint.x - blocPoint.y, 2) + pow(this->getLocation().x - this->getLocation().y, 2));
                    if (dist_h_s < distance) {
                        distance = dist_h_s;

                        target = blocPoint;
                    }
                }
            }
        }
    }

    if (target.x != -1 and target != this->getLocation()) {

        optimalPoint = Player::a_star(this->getLocation(), target, [this](const Point& Start, const Point& End) -> double {

            double h = End - Start + ( End.x < 10? 999:0 );

            std::shared_ptr<MazeCell> endCell = this->getCellFromPoint(End);

            switch (endCell->getBlock()->getKind()) {
                case BORDER:
                    h += 1000;
                    break;
                case UNDEFINED:
                    h += 3;
                    break;
                case GROUND:
                    h += 0.1;
                    break;
                case MATERIAL:
                    auto material = std::dynamic_pointer_cast<Material>(endCell->getBlock());
                    switch (material->getMaterialKind()) {
                        case MaterialKind::STONE:
                            h += endCell->getBlock()->getResistance()/50;
                            break;
                        case MaterialKind::METAL:
                            h += endCell->getBlock()->getResistance()/20;
                            break;
                    }
                    break;
            }

            return h;

        });

        optimalAction = PlayerAction(this->getOrientationToPoint(this->getLocation(), optimalPoint), Interaction(MOVE));

    } else {
        optimalAction = {Orientation(-1), Interaction(-1)};
    }


    return actions[this->chooseAction(observation, actions, optimalAction)];

}


void AstarSeeker::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {

    // Target
    if ((this->getLocation().x == this->getTargetLoc().x) and (this->getLocation().y == this->getTargetLoc().y)) {
        this->forgetTargetLoc(); // Suppression de la target si on y est
    }
    this->setTargetLoc(prev);


    /*std::cout << "MazeMem Update " << this->getName() << std::endl;
    int row = 0;
    for (int i=0; i < getMazeMem().size(); i++) {
        std::cout << getMazeMem().at(i)->getBlock()->getKind();
        row++;
        if (getMazeMem().at(i)->hasPlayer()) {
            std::cout << getMazeMem().at(i)->getPlayer()->getName();
        }
        std::cout << " ";
        if (row >= std::get<0>(prev->getSize())) {
            std::cout << std::endl;
            row = 0;
        }
    }*/

    this->updateMazeMem(prev);
}
