#ifndef HIDE_AND_SEEK_PLAYER_HPP
#define HIDE_AND_SEEK_PLAYER_HPP

#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include <deque>

#include "player/action.hpp"
#include "player/role.hpp"
//#include "maze/observation.hpp"
#include "maze/maze.hpp"
#include "utils/point.hpp"
#include "utils/json.hpp"

class Observation;

/**
 * @brief Abstract Player class
 * 
 */
class Player
{
protected:
    std::string name;
    std::vector<PlayerAction> possibleActions;
    Orientation orientation;
    Point location;
    Point targetLoc;

    // Mémoire interne du labyrinthe
    // Ce met a jour avec les parties et les cases sont initiés a nullptr.
	std::vector<std::shared_ptr<MazeCell>> mazeMem;

	static std::deque<Point> Path(std::unordered_map<Point, Point>& Parents, Point& current);
	static std::deque<Point> expand_point( Point& P );

public:
    Player(const std::string &name);

	virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const = 0;

	virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) = 0;
	virtual Role getRole() const = 0;
	const std::vector<PlayerAction> &getPossibleActions() const;
	const std::string &getName() const;
	const Point &getLocation() const;


	void setLocation(const unsigned int x, const unsigned int y);
	const Orientation getOrientation() const;

    void setOrientation(Orientation orient);
	virtual const std::string toString() const;

    virtual nlohmann::json toJSON() const;

    ~Player();

    friend std::ostream &operator<<(std::ostream &os, const Player &p) {
        return os << p.toString();
    }

    const Point &getTargetLoc() const;
    void forgetTargetLoc();
    void setTargetLoc(const std::shared_ptr<Observation> &observation);
    int chooseAction(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions, const PlayerAction &optimal) const;
    PlayerAction getActionFromTarget() const;
    int findAction(const PlayerAction &action, const std::vector<PlayerAction> &actions) const;
    Orientation inverseOrientation(const Orientation orientation) const;
    Orientation getAdjacentOrientation(const Orientation orientation, const std::vector<PlayerAction> &actions) const;
    Orientation getOrientationToPoint(const Point start, const Point end) const;
    std::shared_ptr<MazeCell> getCellFromPoint(const Point point) const;

    void updateMazeMem(const std::shared_ptr<Observation> &observation);
    const std::vector<std::shared_ptr<MazeCell>> &getMazeMem() const;

    static Point a_star(const Point &A, const Point &B, std::function<double(const Point &, const Point &)> cost_function);
};

// std::ostream &operator<<(std::ostream &os, const Player &p) 

#endif