#define CATCH_CONFIG_MAIN

#include "catch.hpp"

// game includes
#include "game.hpp"
#include "utils/helper.hpp"

// maze includes
#include "factory/mazeFactory.hpp"
#include "maze/custom/basic.hpp"
#include "maze/custom/ConstraintMaze.hpp"
#include "maze/custom/SmartMaze.hpp"

// player includes
#include "factory/playerFactory.hpp"
#include "player/seeker/random.hpp"
#include "player/seeker/expert.hpp"
#include "player/seeker/astar.hpp"
#include "player/hider/random.hpp"
#include "player/hider/expert.hpp"
#include "player/hider/astar.hpp"

namespace Params {

    std::string hiderClassName = "random";
    std::string seekerClassName = "random";
    std::string mazeClassName = "random+";

    unsigned int nGames = 10;
};

// do not change this test!!!
TEST_CASE("check static params", "Game Params")
{
    REQUIRE(GameParams::height == 20);
    REQUIRE(GameParams::width == 20);
    REQUIRE(GameParams::nRounds == 200);
    REQUIRE(GameParams::nSeekers == 2);
    REQUIRE(GameParams::nHiders == 2);
    REQUIRE(GameParams::pctBlock == 0.40);
    REQUIRE(GameParams::pctWood == 0.08);
    REQUIRE(GameParams::pctStone == 0.04);
    REQUIRE(GameParams::pctMetal == 0.02);
}

TEST_CASE("check maze", "basic")
{

    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<BasicMaze>("random");

    Params::mazeClassName = "random";

    auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
    maze->build();

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {

        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    // check maze of current created game
    REQUIRE(Helper::isValid(game));
}

TEST_CASE("check random+ maze", "constraint")
{

    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<ConstraintMaze>("random+");

    Params::mazeClassName = "random+";

    auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
    maze->build();

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {

        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    // check maze of current created game
    REQUIRE(Helper::isValid(game));
}

TEST_CASE("check smart maze", "pattern")
{

    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<SmartMaze>("SmartMaze");

    Params::mazeClassName = "SmartMaze";


    smart_pattern_manager* P= smart_pattern_manager::get_instance();
    P->register_pattern(Pattern_Matrix({
                                               WALL, WALL, WALL,
                                               EMPTY, EMPTY, EMPTY,
                                               WALL, WALL, WALL
                                       }));


    P->register_pattern(Pattern_Matrix({
                                               WALL, EMPTY, WALL,
                                               EMPTY, EMPTY, EMPTY,
                                               WALL, WALL, WALL
                                       }));


    P->register_pattern(Pattern_Matrix({
                                               EMPTY, WALL, EMPTY,
                                               WALL, EMPTY, WALL,
                                               EMPTY, BREAKABLE, EMPTY
                                       }));


    auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
    maze->build();

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for(unsigned int j = 0; j < GameParams::nHiders; j++) {

        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for(unsigned int j = 0; j < GameParams::nSeekers; j++) {

        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    // check maze of current created game
    REQUIRE(Helper::isValid(game));
}

TEST_CASE("check hider victory all random", "random SEEKER vs random HIDER")
{
	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "Test random SEEKER vs random HIDER" << std::endl;
	std::cout << "--------------------------------------------" << std::endl;

	// Need to register expected class
	HiderFactory::registerClass<RandomHider>("random");
	SeekerFactory::registerClass<RandomSeeker>("random");
	MazeFactory::registerClass<ConstraintMaze>("random+");

	Params::mazeClassName= "random+";

	std::map<Role, unsigned int> results;
	results[Role::HIDER] = 0;
	results[Role::SEEKER] = 0;

	for(int i = 0; i < Params::nGames; i++) {

		// create Game and init maze (required call build)
		auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
		maze->build();


		std::cout << "Generated Maze:" << std::endl;
		std::cout << maze->toString() << std::endl;


		Game game(maze, GameParams::nRounds);

		// then add Players (hiders and seekers)
		for(unsigned int j = 0; j < GameParams::nHiders; j++) {

			std::string hider_name = "H" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
			game.addPlayer(hider);
		}

		for(unsigned int j = 0; j < GameParams::nSeekers; j++) {

			std::string seeker_name = "S" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
			game.addPlayer(seeker);
		}

		std::cout << "Game n°" << (i + 1) << " with: " << "{hider: " << Params::hiderClassName << ", seeker: "
				  << Params::seekerClassName << ", maze: " << Params::mazeClassName << "}" << std::endl;

		// before running
		REQUIRE(Helper::isValid(game));
		//if (!Helper::isValid(game)) {
		//    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
		//}

		// run the game
		while(!game.end()) {

			auto player = game.getCurrentPlayer();

			// state is composed of <Observation, vector<PlayerAction>>
			auto state = game.getState(player);
			// get the chosen action by IA
			PlayerAction chosenAction = player->play(state.first, state.second);
			// Play the action in the game and retrieve the new game state
			auto observation = game.step(player, chosenAction);
			// updtae the IA
			player->update(state.first, observation, chosenAction);
		}

		Role winner = game.getCurrentWinner();
		std::cout << "-- Winner is: " << roleToString(winner) << " team in " << game.getNumberOfRounds() << " rounds"
				  << std::endl;

		// update stat
		results[winner] += 1;
	}

	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
	for(auto& k: results) {
		std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
				  << (k.second / (double) Params::nGames) * 100. << "%)" << std::endl;
	}

	// at least 50 percent of victory for hiders
	REQUIRE(results[Role::HIDER] / (double) Params::nGames > 0.5);
}

TEST_CASE("check hider victory expert", "random SEEKER vs expert HIDER")
{
	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "Test random SEEKER vs expert HIDER" << std::endl;
	std::cout << "--------------------------------------------" << std::endl;

	// Need to register expected class
	HiderFactory::registerClass<ExpertHider>("expert");
	SeekerFactory::registerClass<RandomSeeker>("random");
	MazeFactory::registerClass<ConstraintMaze>("random+");

	Params::hiderClassName = "expert";
    Params::seekerClassName = "random";
	Params::mazeClassName= "random+";

	std::map<Role, unsigned int> results;
	results[Role::HIDER] = 0;
	results[Role::SEEKER] = 0;

	for(int i = 0; i < Params::nGames; i++) {

		// create Game and init maze (required call build)
		auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
		maze->build();

		std::cout << "Generated Maze:" << std::endl;
		std::cout << maze->toString() << std::endl;

		Game game(maze, GameParams::nRounds);

		// then add Players (hiders and seekers)
		for(unsigned int j = 0; j < GameParams::nHiders; j++) {

			std::string hider_name = "H" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
			game.addPlayer(hider);
		}

		for(unsigned int j = 0; j < GameParams::nSeekers; j++) {

			std::string seeker_name = "S" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
			game.addPlayer(seeker);
		}

		std::cout << "Game n°" << (i + 1) << " with: " << "{hider: " << Params::hiderClassName << ", seeker: "
				  << Params::seekerClassName << ", maze: " << Params::mazeClassName << "}" << std::endl;

		// before running
		REQUIRE(Helper::isValid(game));
		//if (!Helper::isValid(game)) {
		//    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
		//}

		// run the game
		while(!game.end()) {

			auto player = game.getCurrentPlayer();

			// state is composed of <Observation, vector<PlayerAction>>
			auto state = game.getState(player);
			// get the chosen action by IA
			PlayerAction chosenAction = player->play(state.first, state.second);
			// Play the action in the game and retrieve the new game state
			auto observation = game.step(player, chosenAction);
			// updtae the IA
			player->update(state.first, observation, chosenAction);
		}

		Role winner = game.getCurrentWinner();
		std::cout << "-- Winner is: " << roleToString(winner) << " team in " << game.getNumberOfRounds() << " rounds"
				  << std::endl;

		// update stat
		results[winner] += 1;
	}

	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
	for(auto& k: results) {
		std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
				  << (k.second / (double) Params::nGames) * 100. << "%)" << std::endl;
	}

	// at least 80 percent of victory for hiders
	REQUIRE(results[Role::HIDER] / (double) Params::nGames > 0.8);
}

TEST_CASE("check hider victory astar", "random SEEKER vs astar HIDER")
{
    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "Test random SEEKER vs astar HIDER" << std::endl;
    std::cout << "--------------------------------------------" << std::endl;

    // Need to register expected class
    HiderFactory::registerClass<AstarHider>("astar");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<ConstraintMaze>("random+");

    Params::hiderClassName = "astar";
    Params::seekerClassName = "random";
    Params::mazeClassName= "random+";

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for(int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        std::cout << "Generated Maze:" << std::endl;
        std::cout << maze->toString() << std::endl;

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for(unsigned int j = 0; j < GameParams::nHiders; j++) {

            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for(unsigned int j = 0; j < GameParams::nSeekers; j++) {

            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: " << "{hider: " << Params::hiderClassName << ", seeker: "
                  << Params::seekerClassName << ", maze: " << Params::mazeClassName << "}" << std::endl;

        // before running
        REQUIRE(Helper::isValid(game));
        //if (!Helper::isValid(game)) {
        //    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        //}

        // run the game
        while(!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in " << game.getNumberOfRounds() << " rounds"
                  << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for(auto& k: results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
                  << (k.second / (double) Params::nGames) * 100. << "%)" << std::endl;
    }

    // at least 100 percent of victory for hiders
    REQUIRE(results[Role::HIDER] / (double) Params::nGames >= 1);
}

TEST_CASE("check seeker victory expert", "expert SEEKER vs random HIDER")
{
	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "Test expert SEEKER vs random HIDER" << std::endl;
	std::cout << "--------------------------------------------" << std::endl;

	// Need to register expected class
	HiderFactory::registerClass<RandomHider>("random");
	SeekerFactory::registerClass<ExpertSeeker>("expert");
	MazeFactory::registerClass<ConstraintMaze>("random+");

	Params::seekerClassName = "expert";
	Params::hiderClassName = "random";
    Params::mazeClassName= "random+";

	std::map<Role, unsigned int> results;
	results[Role::HIDER] = 0;
	results[Role::SEEKER] = 0;

	for(int i = 0; i < Params::nGames; i++) {

		// create Game and init maze (required call build)
		auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
		maze->build();

		Game game(maze, GameParams::nRounds);

		// then add Players (hiders and seekers)
		for(unsigned int j = 0; j < GameParams::nHiders; j++) {

			std::string hider_name = "H" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
			game.addPlayer(hider);
		}

		for(unsigned int j = 0; j < GameParams::nSeekers; j++) {

			std::string seeker_name = "S" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
			game.addPlayer(seeker);
		}

		std::cout << "Game n°" << (i + 1) << " with: " << "{hider: " << Params::hiderClassName << ", seeker: "
				  << Params::seekerClassName << ", maze: " << Params::mazeClassName << "}" << std::endl;

		// before running
		REQUIRE(Helper::isValid(game));
		//if (!Helper::isValid(game)) {
		//    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
		//}

		// run the game
		while(!game.end()) {

			auto player = game.getCurrentPlayer();

			// state is composed of <Observation, vector<PlayerAction>>
			auto state = game.getState(player);
			// get the chosen action by IA
			PlayerAction chosenAction = player->play(state.first, state.second);
			// Play the action in the game and retrieve the new game state
			auto observation = game.step(player, chosenAction);
			// updtae the IA
			player->update(state.first, observation, chosenAction);
		}

		Role winner = game.getCurrentWinner();
		std::cout << "-- Winner is: " << roleToString(winner) << " team in " << game.getNumberOfRounds() << " rounds"
				  << std::endl;

		// update stat
		results[winner] += 1;
	}

	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
	for(auto& k: results) {
		std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
				  << (k.second / (double) Params::nGames) * 100. << "%)" << std::endl;
	}

	// at least 50 percent of victory for seekers
	REQUIRE(results[Role::SEEKER] / (double) Params::nGames > 0.8);
}

TEST_CASE("check seeker victory astar", "astar SEEKER vs random HIDER")
{
    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "Test astar SEEKER vs random HIDER" << std::endl;
    std::cout << "--------------------------------------------" << std::endl;

    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<AstarSeeker>("astar");
    MazeFactory::registerClass<ConstraintMaze>("random+");

    Params::hiderClassName = "random";
    Params::seekerClassName = "astar";
    Params::mazeClassName = "random+";

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {

            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: "
                  << "{hider: " << Params::hiderClassName
                  << ", seeker: " << Params::seekerClassName
                  << ", maze: " << Params::mazeClassName
                  << "}" << std::endl;

        // before running
        REQUIRE(Helper::isValid(game));
        //if (!Helper::isValid(game)) {
        //    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        //}

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in "
                  << game.getNumberOfRounds() << " rounds" << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
                  << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }

    // at least 100 percent of victory for seekers
    REQUIRE(results[Role::SEEKER] / (double)Params::nGames >= 1);
}

// - 50 percent of victory vs random with your seeker and/or hider
// - 80 percent of victory vs random with your seeker and/or hider
// - 100 percent?