#ifndef HIDE_AND_SEEK_HIDER_PLAYER_ASTAR_HPP
#define HIDE_AND_SEEK_HIDER_PLAYER_ASTAR_HPP

#include <string>
#include <memory>

#include "player/hider/hider.hpp"
#include "player/action.hpp"

/**
 * @brief Astar Hider class
 * 
 */
class AstarHider : public Hider
{
public:
    AstarHider(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);

    ~AstarHider();
};

#endif