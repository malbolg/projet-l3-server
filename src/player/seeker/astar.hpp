#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_ASTAR_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_ASTAR_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief Astar Seeker class
 * 
 */
class AstarSeeker : public Seeker
{
public:
    AstarSeeker(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);

    ~AstarSeeker();
};

#endif