#ifndef HIDE_AND_SEEK_MAZE_HPP
#define HIDE_AND_SEEK_MAZE_HPP

#include <vector>
#include <memory>
#include "utils/json.hpp"

#include "maze/cell.hpp"
#include "maze/observation.hpp"
#include "player/player.hpp"
#include "player/action.hpp"
#include "utils/randomGenerator.hpp"
#include "utils/point.hpp"


enum case_status {
	WALL, EMPTY, FILLED, BREAKABLE
};

class Player;

/**
 * @brief Abstract Maze class representation
 * 
 */
class Maze
{
protected:
    unsigned int width;
    unsigned int height;
    std::vector<std::shared_ptr<MazeCell>> cells;

    void addActions(std::vector<PlayerAction> &actions, int cellIndex, std::shared_ptr<Player> player, 
            Orientation orient) const;

	static bool hasIsolatedCell(std::vector<std::vector<case_status>> case_status_maze);
	static bool isTraversable(case_status c);
public:
	static std::string case_stat_to_string(case_status c);
	Maze(unsigned int width, unsigned int height);

    // by default empty... (avoid abstract because of fromJSON...)
    virtual void build() {};

    bool addPlayer(const std::shared_ptr<Player> player, unsigned int x, unsigned int y);
    bool addPlayer(const std::shared_ptr<Player> player);
    std::shared_ptr<Observation> doPlayerAction(const std::shared_ptr<Player> player, PlayerAction action);

    std::vector<PlayerAction> getActionsOfPlayer(const std::shared_ptr<Player> player) const;
    std::shared_ptr<Observation> getObservationOfPlayer(const std::shared_ptr<Player> player) const;
    bool setCells(std::vector<std::shared_ptr<MazeCell>> cells);
    bool isVisible(const std::shared_ptr<Player> &player, Point coordinate) const;
    const std::shared_ptr<MazeCell> &getCell(unsigned int x, unsigned int y) const;
    const std::vector<std::shared_ptr<MazeCell>> &getCells() const;
    
    std::string toString() const;
    const nlohmann::json toJSON() const;

    const unsigned int getHeight() const {
        return height;
    }

	const unsigned int getWidth() const {
        return width;
    }

    ~Maze();

    friend std::ostream &operator<<(std::ostream &os, const Maze &m) { 
        return os << m.toString();
    }

};

#endif