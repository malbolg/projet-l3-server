#include <iostream>
#include "player/player.hpp"
#include "utils/point.hpp"
#include "game.hpp"

int main() {
	std::deque<Point> Points;
	Point Start(1, 1), End(18, 18), Current(Start);
	Points.push_front(Current);
	while(Current != End) {
		Current = Player::a_star(Current, End, [](const Point& Start, const Point& End) -> double {
			return End - Start + ( End.x < 10? 999:0 );
		});
		Points.push_front(Current);
	}
	char** c_array = new char* [GameParams::height];
	for(int i = 0; i < GameParams::height; i++)
		c_array[i] = new char[GameParams::width];

	for(int y = 0; y < GameParams::height; y++) {
		for(int x = 0; x < GameParams::width; x++) {
			if(x == 0 ||  x == GameParams::width - 1 ) {
				c_array[y][x]='|';
				continue;
			} else if ( y == 0 || y == GameParams::height - 1 ){
				c_array[y][x]='_';
				continue;
			}


			c_array[y][x] = ' ';
		}
	}
	for(const auto& P: Points) {
		if( Start == P ) {
			c_array[P.y][P.x]='A';
			continue;
		}
		if( End == P ) {
			c_array[P.y][P.x]='B';
			continue;
		}
		c_array[P.y][P.x] = '*';
	}

	for(int y = 0; y < GameParams::height; y++) {
		for(int x = 0; x < GameParams::width; x++) {
			std::cout << c_array[y][x];
		}
		std::cout << std::endl;
	}
}
