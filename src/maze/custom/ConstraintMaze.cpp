#include "ConstraintMaze.hpp"
#include "maze/block/border.hpp"
#include "maze/block/material/custom.hpp"
#include "maze/block/ground.hpp"
#include <stack>
#include <queue>

void ConstraintMaze::build() {
//	Taille interne du labyrinthe ( sans les bordures )
	const int maze_width = width - 2;
	const int maze_height = height - 2;

//	Total des blocks.
	const int total_blocks = width * height;

//	Taille avec bordures vs sans les bordures
	const int border_blocks = total_blocks - (maze_width * maze_height);

//	Nombre de blocs solides à placer au total, entre 0 et GameParams::pctBlock
	int total_solid_blocks_left = GameParams::pctBlock * total_blocks - border_blocks;

//	Positions de tout les murs storés dans l'ordre de leur génération.
//	Au total, il aura une taille final de total_solid_blocks, ici égale à
//	total_solid_blocks_left
	std::vector<std::pair<short, short>> walls_pos;

//	Idem mais pour chaque type
	int wood_left = (total_blocks * GameParams::pctWood);
	int metal_left = (total_blocks * GameParams::pctMetal);
	int stone_left = (total_blocks * GameParams::pctStone);

//	représentation du labyrinthe sous forme de mur/sol/'filled' ( case connectés )
//	ne compte pas les bordures.
	std::vector<std::vector<case_status>> cell_repr;

//	Refactor possible?
	for(int i = 0; i < maze_height; i++) {
		std::vector<case_status> line;
		for(int j = 0; j < maze_width; j++)
			line.push_back(case_status::EMPTY);
		cell_repr.push_back(line);
	}

	while(total_solid_blocks_left) {
		short new_wall_x = rGenerator->getRandomIntInterval(0, maze_width - 1);
		short new_wall_y = rGenerator->getRandomIntInterval(0, maze_height - 1);
		case_status& new_wall = cell_repr[new_wall_y][new_wall_x];
		if(new_wall == case_status::EMPTY) {
			new_wall = case_status::WALL;

			if(hasIsolatedCell(cell_repr))
				new_wall = case_status::EMPTY;
			else {
//				construction implicite du std::pair.
//				+1 car on doit ici compter les bordures alors que notre representation ne les comptes pas.
				walls_pos.emplace_back(new_wall_x + 1, new_wall_y + 1);
				total_solid_blocks_left--;
			}
		}
	}

	cells.resize(height * width);

//		Génération du labyrinthe
//	Barrières verticales.
	for(uint border_y: {(uint) 0, height - 1}) {
		for(uint x = 0; x < width; x++) {
			cells[border_y * width + x] = std::make_shared<MazeCell>(std::make_shared<Border>());
		}
	}

//	Barrières horizontales.
	for(uint border_x: {(uint) 0, width - 1}) {
		for(uint y = 0; y < width; y++) {
			cells[y * width + border_x] = std::make_shared<MazeCell>(std::make_shared<Border>());
		}
	}

//	Génération des blocks.
	std::shared_ptr<Block> block;
	for(const auto& wall_pos: walls_pos) { // walls_pos.size() == Gamepct::pctBlock*width*height
		if(wood_left > 0) {
			block = std::make_shared<Wood>();
			wood_left--;
		}
		else if(stone_left > 0) {
			block = std::make_shared<Stone>();
			stone_left--;
		}
		else if(metal_left > 0) {
			block = std::make_shared<Metal>();
			metal_left--;
		}
		else { block = std::make_shared<Border>(); }

		cells[wall_pos.second * width + wall_pos.first] = std::make_shared<MazeCell>(block);
	}

//	Génération du sol
	for(auto& c: cells) {
		if(c == nullptr) {
			c = std::make_shared<MazeCell>(std::make_shared<Ground>());
		}
	}
}
