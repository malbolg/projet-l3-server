#ifndef HIDE_AND_SEEK_CONSTRAINTMAZE_HPP
#define HIDE_AND_SEEK_CONSTRAINTMAZE_HPP

#include "maze/maze.hpp"
#include "game.hpp"

class ConstraintMaze : public Maze {
private:
	// Retrieve the current random generator
	std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
public:
	ConstraintMaze(unsigned int width, unsigned int height) : Maze(width, height) {};

	virtual void build();

	~ConstraintMaze() {};
};


#endif //HIDE_AND_SEEK_CONSTRAINTMAZE_HPP
