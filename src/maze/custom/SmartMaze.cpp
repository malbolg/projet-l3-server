#include "SmartMaze.hpp"
#include "game.hpp"
#include "maze/maze.hpp"
#include "utils/randomGenerator.hpp"
#include "maze/block/material/custom.hpp"
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include <random>
#include <stdexcept>
#include <queue>
#include <stack>


Pattern_Matrix smart_pattern_manager::rotate_pattern(Pattern_Matrix& mat) {
	return Pattern_Matrix({
								  mat[6], mat[3], mat[0], mat[7], mat[4], mat[1], mat[8], mat[5],
								  mat[2]
						  });
}

Point Pattern_Matrix::convert_to_point(ushort pos) {
	return {(int) pos % 3, (int) pos / 3};
}

///////
// smart_pattern_manager defs
///////



void smart_pattern_manager::register_pattern(Pattern_Matrix pattern) {
//	si le pattern existe, il a déja été enregistré, on évite ici les duplicat.
	if(std::find(patterns.begin(), patterns.end(), pattern) != patterns.end())
		return;

	for(int rotation = 0; rotation < 360; rotation += 90) {
		Pattern_Matrix B;

		for(uint pos = 0; pos < pattern.size(); pos++)
			B[pos] = pattern[pos];

//		Ici également, certain pattern sont symmétrique, on cherche donc si le pattern existe déja
//		Étant donné que cela arrive lors d'une symmétrie, le reste des patterns est nécéssairement
//		composé de duplicat, donc, nécéssité de break; au lieu de continue;
		if(std::find(patterns.begin(), patterns.end(), B) != patterns.end())
			break;

		patterns.push_back(B);
		pattern = rotate_pattern(pattern);
	}
}

std::string smart_pattern_manager::to_string() const {
	std::string buf;

	std::for_each(patterns.cbegin(), patterns.cend(), [&buf](const Pattern_Matrix& B) {
		for(int i = 0; i < Pattern_Matrix::get_size(); i++) {
			buf += Maze::case_stat_to_string(B[i]);
			if(i % 3 == 2) buf += '\n';
		}

		buf += '\n';
	});

	return buf;
}

Pattern_Matrix smart_pattern_manager::get_random() const {
	return patterns[RandGenerator::getInstance()->getRandomInt(patterns.size() - 1)];
}
////////////////////////


////////
// SmartMaze defs
///////
/** @brief generates a maze using a set of pattern randomly and ensures that GameParams are enforced
 */
void SmartMaze::build() { // TODO: FIX
//	Taille interne du labyrinthe
	const uint maze_width = width - 2;
	const uint maze_height = height - 2;

//	taille des patterns, respectivement coté et aire
	const uint pattern_side_size = sqrt(Pattern_Matrix::get_size());

//	Total des blocks.
	const int total_blocks = width * height;

//	Taille avec bordures - sans les bordures
	const int border_blocks = total_blocks - (maze_width * maze_height);

//	Idem mais pour chaque type
	int wood_left = (total_blocks * GameParams::pctWood);
	int metal_left = (total_blocks * GameParams::pctMetal);
	int stone_left = (total_blocks * GameParams::pctStone);

//	Nombre de blocs à placer au total ( en somme )
	const int total_solid = GameParams::pctBlock * total_blocks - border_blocks;
	const int total_breakables = wood_left + metal_left + stone_left;
	const int total_unbreakables = total_solid - total_breakables;

//	Position des murs et cellules a généré ( sur une grille réduite ( ie: 1x1 -> 3x3 ) )
	std::deque<Point> unbreak_walls, break_walls;
	std::vector<Point> toGenerate;
	std::deque<Point> generated;

//	États des cases
	std::vector<std::vector<case_status>> case_status_cells;

	for(int y = 0; y < maze_height; y++) {
		case_status_cells.emplace_back();

		for(int x = 0; x < maze_width; x++)
			case_status_cells[y].push_back(EMPTY);
	}


	for(uint y = 0; y < (maze_height / pattern_side_size); y++)
		for(uint x = 0; x < (maze_width / pattern_side_size); x++)
			toGenerate.emplace_back(x, y);


	auto shuffler = std::mt19937(std::random_device()());
	int breakable_placed = 0;
	int unbreakable_placed = 0;

//	tant que l'on n'a pas 40% de block placé tous confondus et
//	le nombre requis de block destructible n'est pas atteint ( car on ne peut les convertir en indestructible )
//	on continue de générer des patterns.
	while(total_solid> breakable_placed+unbreakable_placed || total_breakables > breakable_placed) {
		Point P = toGenerate[RandGenerator::getInstance()->getRandomInt(toGenerate.size() - 1)];
		Point P_real = P * pattern_side_size;
		Pattern_Matrix pattern = smart_pattern_manager::get_instance()->get_random();

//		Applique le pattern selectionné aléatoirement sur la représentation du labyrinthe en "case_status"
		for(uint y = 0; y < pattern_side_size; y++) {
			for(uint x = 0; x < pattern_side_size; x++) {
				auto prev_type_of_cell = case_status_cells[y + P_real.y][x + P_real.x];
				auto next_type_of_cell = pattern[y * pattern_side_size + x];

				if(next_type_of_cell == WALL) unbreakable_placed++;
				if(prev_type_of_cell == WALL) unbreakable_placed--;
				if(next_type_of_cell == BREAKABLE) breakable_placed++;
				if(prev_type_of_cell == BREAKABLE) breakable_placed--;


				case_status_cells[P_real.y + y][P_real.x + x] = next_type_of_cell;
			}
		}

//		on recommance si il y a des cellules isolées
//		peut-être source de boucle infinie
		if(hasIsolatedCell(case_status_cells)) {
			for(uint y = 0; y < pattern_side_size; y++)
				for(uint x = 0; x < pattern_side_size; x++)
					case_status_cells[P_real.y + y][P_real.x + x] = EMPTY;

		}
	}


	for(uint y = 0; y < maze_height; y++) {
		for(uint x = 0; x < maze_width; x++) {
			auto type_of_cell = case_status_cells[y][x];

			switch(type_of_cell) {
				case WALL:
//							+1 car on convertit vers un labyrinthe avec bordures.
					unbreak_walls.emplace_back(1 + x, 1 + y);
					break;
				case BREAKABLE:
//					+1 car on convertit vers un labyrinthe avec bordures.
					break_walls.emplace_back(1 + x, 1 + y);
				default:
					break;
			}
		}
	}

	std::shuffle(unbreak_walls.begin(), unbreak_walls.end(), shuffler);
	std::shuffle(break_walls.begin(), break_walls.end(), shuffler);

//	On peut rendre des murs indestructibles destructible, mais pas l'inverse pour préserver
//	l'access a toute les cellules "libres"
	while(unbreak_walls.size() > total_unbreakables) {
		if(break_walls.size() < total_breakables) {
			break_walls.push_back(unbreak_walls.back());
			unbreak_walls.pop_back();
		} else break;
	}

//	refactor possible
//	on force les tailles des vecteurs de murs a générer.
	if((const int)total_solid < unbreak_walls.size() + break_walls.size()) {
		unbreak_walls.resize(total_unbreakables);
		break_walls.resize(total_breakables);
	}


	//		Génération du labyrinthe
	cells.resize(height * width);
//	Barrières verticales.
	for(uint border_y: {(uint) 0, height - 1}) {
		for(uint x = 0; x < width; x++) {
			cells[border_y * width + x] = std::make_shared<MazeCell>(std::make_shared<Border>());
		}
	}

//	Barrières horizontales.
	for(uint border_x: {(uint) 0, width - 1}) {
		for(uint y = 0; y < width; y++) {
			cells[y * width + border_x] = std::make_shared<MazeCell>(std::make_shared<Border>());
		}
	}

//	Génération des blocks.
	for(const auto& wall_pos: break_walls) { // walls_pos.size() == Gamepct::pctBlock*width*height
		std::shared_ptr<Block> block;
		if(wood_left > 0) {
			block = std::make_shared<Wood>();
			wood_left--;
		} else if(stone_left > 0) {
			block = std::make_shared<Stone>();
			stone_left--;
		} else if(metal_left > 0) {
			block = std::make_shared<Metal>();
			metal_left--;
		} else { throw std::runtime_error("too many breakable walls to place!"); }

		cells[wall_pos.y * width + wall_pos.x] = std::make_shared<MazeCell>(block);
	}

//	Génération blocks invincibles.
	for(const auto& wall_pos: unbreak_walls) {
		std::shared_ptr<Block> block = std::make_shared<Border>();
		cells[wall_pos.y * width + wall_pos.x] = std::make_shared<MazeCell>(block);
	}

//	Génération du sol
	for(auto& c: cells) {
		if(!c)
			c = std::make_shared<MazeCell>(std::make_shared<Ground>());
	}

}

std::vector<Point> SmartMaze::expand_growing_tree(Point& P) {
	std::vector<Point> vec;
	const int pattern_side_size = sqrt(Pattern_Matrix::get_size());
	if(P.x < ((GameParams::width / pattern_side_size) - 1))
		vec.emplace_back(P.x + 1, P.y);

	if(P.x > 1)
		vec.emplace_back(P.x - 1, P.y);

	if(P.y < (GameParams::height / pattern_side_size) - 1)
		vec.emplace_back(P.x, P.y + 1);

	if(P.y > 1)
		vec.emplace_back(P.x, P.y - 1);

	// On rend l'ordre des points aléatoire.
	std::shuffle(vec.begin(), vec.end(), std::mt19937(std::random_device()()));

	return vec;
}