#include "game.hpp"
#include "factory/playerFactory.hpp"
#include "factory/mazeFactory.hpp"

// specific incluces
#include "maze/custom/basic.hpp"
#include "maze/custom/ConstraintMaze.hpp"
#include "player/seeker/random.hpp"
#include "player/seeker/expert.hpp"
#include "player/seeker/astar.hpp"
#include "player/hider/random.hpp"
#include "player/hider/expert.hpp"
#include "player/hider/astar.hpp"

// test game validity
#include "utils/helper.hpp"
#include "maze/custom/ConstraintMaze.hpp"
#include "maze/custom/SmartMaze.hpp"

// default params
namespace Params {

	std::string hiderClassName = "astar";
	std::string seekerClassName = "astar";
	std::string mazeClassName = "smart";
	int nGames = 100;
};

int main(int argc, char* argv[]) {
    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    HiderFactory::registerClass<ExpertHider>("expert");
    HiderFactory::registerClass<AstarHider>("astar");

    SeekerFactory::registerClass<RandomSeeker>("random");
	SeekerFactory::registerClass<ExpertSeeker>("expert");
    SeekerFactory::registerClass<AstarSeeker>("astar");

	MazeFactory::registerClass<BasicMaze>("random");
	MazeFactory::registerClass<ConstraintMaze>("random+");
	MazeFactory::registerClass<SmartMaze>("smart");

	smart_pattern_manager* P = smart_pattern_manager::get_instance();
	P->register_pattern(Pattern_Matrix({
											   WALL, WALL, WALL,
											   EMPTY, EMPTY, EMPTY,
											   WALL, WALL, WALL
									   }));

	P->register_pattern(Pattern_Matrix({
											   WALL, BREAKABLE, WALL,
											   WALL, EMPTY, BREAKABLE,
											   WALL, EMPTY, BREAKABLE
									   }));

	P->register_pattern(Pattern_Matrix({
											   WALL, EMPTY, WALL,
											   EMPTY, WALL, EMPTY,
											   WALL, EMPTY, WALL
									   }));


	P->register_pattern(Pattern_Matrix({
											   EMPTY, WALL, EMPTY,
											   WALL, EMPTY, WALL,
											   EMPTY, BREAKABLE, EMPTY
									   }));


	std::map<Role, unsigned int> results;
	results[Role::HIDER] = 0;
	results[Role::SEEKER] = 0;

	for(int i = 0; i < Params::nGames; i++) {

		// create Game and init maze (required call build)
		auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width,
										GameParams::height);
		maze->build();

		/*std::string string_maze = maze->toString();
		std::cout << "Generated Maze:" << std::endl << string_maze << std::endl;
		std::cout << "percent of unbreakable walls: " <<
				  std::count(string_maze.cbegin(), string_maze.cend(),
							 '+') / (double)(GameParams::width * GameParams::height)
				  << std::endl;
		std::cout << "percent of breakable walls: " <<
				  std::count(string_maze.cbegin(), string_maze.cend(),
							 'X') / (double)(GameParams::width * GameParams::height)
				  << std::endl;
		std::cout << "------------------------------------------------------" << std::endl;*/

		Game game(maze, GameParams::nRounds);

		// then add Players (hiders and seekers)
		for(unsigned int j = 0; j < GameParams::nHiders; j++) {

			std::string hider_name = "H" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName,
																 hider_name);
			game.addPlayer(hider);
		}

		for(unsigned int j = 0; j < GameParams::nSeekers; j++) {

			std::string seeker_name = "S" + std::to_string(j + 1);
			// only need the player representation server side
			std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName,
																   seeker_name);
			game.addPlayer(seeker);
		}

		std::cout << "Game n°" << (i + 1) << " with: " << "{hider: " << Params::hiderClassName
				  << ", seeker: " << Params::seekerClassName << ", maze: " << Params::mazeClassName
				  << "}" << std::endl;

		// before running
		if(!Helper::isValid(game)) {
			std::cout << "Game is not valid: unexpected maze built..." << std::endl;
			exit(0);
		}

		// run the game
		while(!game.end()) {

			auto player = game.getCurrentPlayer();

			// state is composed of <Observation, vector<PlayerAction>>
			auto state = game.getState(player);
			// get the chosen action by IA
			PlayerAction chosenAction = player->play(state.first, state.second);
			// Play the action in the game and retrieve the new game state
			auto observation = game.step(player, chosenAction);
			// updtae the IA
			player->update(state.first, observation, chosenAction);
		}

		Role winner = game.getCurrentWinner();
		std::cout << "-- Winner is: " << roleToString(winner) << " team in "
				  << game.getNumberOfRounds() << " rounds" << std::endl;

		// update stat
		results[winner] += 1;
	}

	std::cout << "--------------------------------------------" << std::endl;
	std::cout << "End of simulation... Results over " << Params::nGames << " games are: "
			  << std::endl;
	for(auto& k: results) {
		std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
				  << (k.second / (double) Params::nGames) * 100. << "%)" << std::endl;
	}
}