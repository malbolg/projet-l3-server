#ifndef HIDE_AND_SEEK_SMARTMAZE_HPP
#define HIDE_AND_SEEK_SMARTMAZE_HPP

#include <map>
#include <vector>
#include "utils/point.hpp"
#include "maze/maze.hpp"


//using Pattern_Matrix = std::array<bool, 9>;

// Definition du type représentant les patterns de murs/cases vides
// ///////////////////
class Pattern_Matrix : public std::array<case_status, 9> {
public:
	Pattern_Matrix(): std::array<case_status,9>({EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}) {};

	static int get_size() { return 9; }

	explicit Pattern_Matrix(std::array<case_status, 9> init) : std::array<case_status, 9>(init) {};

	/** @brief	Convert the position of a pattern's cell to a local Point */
	static Point convert_to_point( ushort pos ) ;
};


class smart_pattern_manager {
private:
	std::vector<Pattern_Matrix> patterns;

	/** @brief rotate a pattern clockwise
	 *
	 * @param mat a std::array<bool, 9> representing a pattern
	 * @return
	 */
	static Pattern_Matrix rotate_pattern(Pattern_Matrix& mat);
	smart_pattern_manager()= default;
public:

	void operator=(const smart_pattern_manager&)= delete;
	smart_pattern_manager(const smart_pattern_manager&)= delete;

	static smart_pattern_manager* get_instance() {
		static smart_pattern_manager S;
		return &S;
	}

	/** @brief a function to register new pattern into the class and all their rotations */
	void register_pattern(Pattern_Matrix pattern);

	std::string to_string() const;

	Pattern_Matrix get_random() const;
};


// ////
// SmartMaze defs
// ////
class SmartMaze : public Maze {
private:
	static std::vector<Point> expand_growing_tree(Point& P);

public:
	SmartMaze(uint width, uint height) : 	Maze( width, height ) {}

	void build() override;
};


#endif //HIDE_AND_SEEK_SMARTMAZE_HPP
