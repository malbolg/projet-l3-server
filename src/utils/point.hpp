#ifndef HIDE_AND_SEEK_UTILS_POINT_HPP
#define HIDE_AND_SEEK_UTILS_POINT_HPP

#include "utils/json.hpp"

/**
 * @brief Point structure for location into Maze
 * 
 */
struct Point {
	int x;
	int y;

	Point(int x, int y) : x(x), y(y) {}

	// non expected location into Maze by default
	Point() {
		x = -1;
		y = -1;
	}

	bool operator==(const Point& p) const {
		return x == p.x && y == p.y;
	}

	bool operator!=(const Point& p) const {
		return !(x == p.x && y == p.y);
	}

	double operator-(const Point& End) const {
		return (double)sqrt(pow( x - End.x, 2) + pow(y - End.y, 2));
	}

	Point operator*(int i) const {
		return { this->x*i, this->y*i };
	}

	Point operator/(int i) const {
		return { this->x/i, this->y/i };
	}

	nlohmann::json toJSON() const {
		nlohmann::json json_data;

		json_data["x"] = x;
		json_data["y"] = y;

		return json_data;
	}
};

// Pour une utilisation avec unordered map
// voir https://en.cppreference.com/w/cpp/utility/hash
// voir https://www.cplusplus.com/reference/unordered_map/unordered_map/
//		-> CTRL + F => return h1 ^ (h2 << 1); // or use boost::hash_combine
namespace std {

	template<>
	struct hash<Point> {
		std::size_t operator()(const Point& k) const {
			using std::size_t;
			using std::hash;
			using std::string;

			// Combine les hash des positions sur les différents axes.
			return (hash<int>()(k.x) ^ (hash<int>()(k.y) << 1)) >> 1;
		}
	};

}

#endif